# Inspiration  

The open call for experimental publishing:  
[https://copim.pubpub.org/pub/expub-pilot-faqs/release/1](https://copim.pubpub.org/pub/expub-pilot-faqs/release/1)  

Look for experimental publishing workshops on this page:  
[https://copim.pubpub.org/copim-events](https://copim.pubpub.org/copim-events)

Here a the three workshops:  
_ https://copim.pubpub.org/pub/experimental-publishing-workshop-part-1/release/2  
_ https://copim.pubpub.org/pub/experimental-publishing-workshop-part-2/release/2  
_ https://copim.pubpub.org/pub/books-contain-multitudes-part-3-technical-workflows-tools-experimental-publishing/release/4  

book:  
_ https://copim.pubpub.org/pub/combinatorial-books-documentation-open-source-tools-platforms-post5/release/1?readingCollection=ace58019

**Press:**

1. Media studies press: I am in correspondence with Jeff Pooley about something else 😉, my article in Big Data & Society (Open Access). He publishes the History of Media Studies... all good. They have a connection to Google Scholar, as is PubPub
https://www.pubpub.org/about

2. Meson Press: I am probably most familiar with them, because of Leuphana, reading and knowing many of the authors...are they not also connected to University of Minnesota Press?

3. Punctum Books: Also familiar, know their publications and love their Vision and Open Source Tools.

4. Goldsmiths: Not so familiar, it seems they have an embargo period, would that be applicable here?

5. University of Westminster press: I am familiar with them through their series CDSMS, edited by Christian Fuchs. 

https://www.uwestminsterpress.co.uk/site/books/series/critical-digital-and-social-media-studies/

6. UCL: Publishing Studies: I had seen this before:https://www.uclpress.co.uk/collections/publishing-studies/products/84084. Media Studies:https://www.uclpress.co.uk/collections/media-studies, this book recently came up on my radar b/c Niels Bruger is my colleague and the project deals with web histories (search).https://www.uclpress.co.uk/collections/media-studies/products/84067


Some other interesting projects:
https://prepostprint.org/resources/
