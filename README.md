# Re:Search Codex
### A public repository for the *Re:Search Codex* project  

**Project summary (2–3 sentences)**  
This experimental publication project will capture search results received by diverse groups of people through methods of screenshotting and web scraping, displaying two forms of data visualisations.  
The results will be combined into an experimental book format that makes permanent the search results, with annotations and a web ‘re:search’ platform that images and contextualises the results, which can be constantly updated. Serving as a mneumonic register for people’s search interactions, the ‘experimental book’ will lead to greater understanding and comprehension of dynamic technologies that deliver information, as well as providing opportunities for future open source software developments.

### How to install viewer dependencies  
This is how to start the viewer ..  
_ cd viewer  
_ npm install

### How to start viewer  
This is how to start the viewer. If you are already in the folder "viewer", then ...  
_ npm run dev  

---

### Look for inspiration and resources in *Resources.md* file!  

