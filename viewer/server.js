const express = require('express');
// const path = require('path');
// const ejs = require('ejs');
const cors = require('cors');
const app = express();
const uploadMiddleware = require('./middlewares/uploadMiddleware');


// set the view engine to ejs
app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/static'));

// dont use body parser anymore
// https://stackoverflow.com/questions/19917401/error-request-entity-too-large
app.use(express.json({ limit: '80mb' }));
app.use(express.urlencoded({ limit: '80mb', extended: true })); // true }))
app.use(cors());


////////////////////////////////////////////////
// index express route
app.get('/', async (req, res) => {
  res.render('index', { text: "HELLO world!" });
});


////////////////////////////////////////////////
// index express route
app.post('/sendFiles', uploadMiddleware, async (req, res) => {

  console.log("sendFiles was hit!");
  // res.json({ message: "sendFiles was hit!" });

  // https://levelup.gitconnected.com/complete-guide-to-upload-multiple-files-in-node-js-using-middleware-3ac78a0693f3
  // Handle the uploaded files here
  // Access the uploaded files using req.files
  // Process and store the files as required
  // Send an appropriate response to the client
  
  // Handle the uploaded files
  const files = [ req.file1, req.file2 ];
  console.log("files:", files);
  console.log("req.file1:", req.file1);

  // Process and store the files as required
  // For example, save the files to a specific directory using fs module
  files.forEach((file) => {
    const filePath = `uploads/${file.filename}`;
    fs.rename(file.path, filePath, (err) => {
      if (err) {
        // Handle error appropriately and send an error response
        return res.status(500).json({ error: 'Failed to store the file' });
      }
    });
  });

  // Send an appropriate response to the client
  res.status(200).json({ message: 'File upload successful' });
});


// start listening
const PORT = 3000;
app.listen(PORT, function() {
  console.log(`Server is listening on port ${PORT}`);
});