import { getRandomColor } from "./functions.js";
import { Url } from "./Url.js";

export class TextEngine {


  //////////////////////////////////////////
  // this class holds all information on sentences, and the specific pieces of sentences
  constructor() {

    // result arrays
    this.resultsLeft = [];
    this.resultsRight = [];
  }


  //////////////////////////////////////////
  // put some demo txt in arrays ...
  init() {

    this.resultsLeft.push(
      new Url("https://google.com/?q=someQueryStuff&hl=somethingElse"),
      new Url("https://google.com/?q=some&hl=eng&zone=123"),
      new Url("https://google.com/?q=some&hl=somethingElse"),
      new Url("https://www.microsoft.com/en-us/bing?ep=140&es=31&form=MA13FV")
    )
    console.log("this.resultsLeft:", this.resultsLeft);

    this.resultsRight.push(
      new Url("https://dr.dk/?q=someQueryStuff&hl=somethingElseEntirely"),
      new Url("http://dr.dk/?q=some&hl=somethingElse&lng=10.0"),
      new Url("https://mail.google.com/mail/u/0/#inbox/KtbxLzGDZPPLjwDjxgDctbMHJtjtTMjlCg")
    )
    console.log("this.resultsRight:", this.resultsRight);
     
    // make some analys!
    this.analyse();
  }


  //////////////////////////////////////////
  // start processing the results here
  analyse() {

    // for(let c=0; c < this.resultsLeft.length; c++) {
    //   this.resultsLeft[c].analyse();
    // }

    for (let url of this.resultsLeft) { url.analyse() }
    for (let url of this.resultsRight) { url.analyse() }

    // for(let c=0; c < this.resultsRight.length; c++) {
    //   this.resultsRight[c].analyse();
    // }
  }


}