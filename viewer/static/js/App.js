import { RenderEngine2D } from './RenderEngine2D.js';
import { TextEngine } from './TextEngine.js';


///////////////////////////////////////
export class App {

  
  ///////////////////////////////////////
  // constructor
  constructor() {
    this.renderer = null;
    this.txt = null;
  }


  ///////////////////////////////////////
  // initialize the app
  init() {

    console.log("App init was run!");

    // setup text result renderer
    this.renderer = new RenderEngine2D();
    this.renderer.init();

    // setup the text handler engine
    this.txt = new TextEngine();
    this.txt.init();

  }


}