import { getRandomColor } from "./functions.js";
import { Url } from "./Url.js";

export class TextEngine {


  //////////////////////////////////////////
  // this class holds all information on sentences, and the specific pieces of sentences
  constructor() {

    // result arrays
    this.resultsLeft = [];
    this.resultsRight = [];
  }


  //////////////////////////////////////////
  // put some demo txt in arrays ...
  init() {

    this.resultsLeft.push(
      new Url("https://google.com/?q=someQueryStuff&hl=somethingElse"),
      new Url("https://google.com/?q=some&hl=somethingElse")
    )
    console.log("this.resultsLeft:", this.resultsLeft);

    this.resultsRight.push(
      new Url("https://dr.dk/?q=someQueryStuff&hl=somethingElseEntirely"),
      new Url("https://dr.dk/?q=some&hl=somethingElse&lng=10.0")      
    )
    console.log("this.resultsRight:", this.resultsRight);
     
    // make some analys!


    /*
    // testing some more ...
    this.linesLeft = [
      // first array!
      [
        {
          text: "https://google.com/",
          type: "basic",
          hoverText: "hovering now",
          color: getRandomColor()
        },
        {
          text: "?",
          type: "queryStart",
          hoverText: "hovering query start now",
          color: getRandomColor()
        },
        {
          text: "q=someQueryStuff",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        },
        {
          text: "&",
          type: "ampersand",
          hoverText: "hovering amp now",
          color: getRandomColor()     
        },
        {
          text: "hl=somethingElse",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        }
      ],
      // SECOND array!
      [
        {
          text: "https://google.com/",
          type: "basic",
          hoverText: "hovering now",
          color: getRandomColor()
        },
        {
          text: "?",
          type: "queryStart",
          hoverText: "hovering query start now",
          color: getRandomColor()
        },
        {
          text: "q=some",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        },
        {
          text: "&",
          type: "ampersand",
          hoverText: "hovering amp now",
          color: getRandomColor()      
        },
        {
          text: "hl=somethingElse",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        }
      ]
    ];



    // testing some more, FOR HTE RIGHT SIDE!
    this.linesRight = [
      // first array!
      [
        {
          text: "https://google.com/",
          type: "basic",
          hoverText: "hovering now",
          color: getRandomColor()
        },
        {
          text: "?",
          type: "queryStart",
          hoverText: "hovering query start now",
          color: getRandomColor()
        },
        {
          text: "q=someQueryStuff",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        },
        {
          text: "&",
          type: "ampersand",
          hoverText: "hovering amp now",
          color: getRandomColor()     
        },
        {
          text: "hl=somethingElse",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        }
      ],
      // SECOND array!
      [
        {
          text: "https://google.com/",
          type: "basic",
          hoverText: "hovering now",
          color: getRandomColor()
        },
        {
          text: "?",
          type: "queryStart",
          hoverText: "hovering query start now",
          color: getRandomColor()
        },
        {
          text: "q=some",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        },
        {
          text: "&",
          type: "ampersand",
          hoverText: "hovering amp now",
          color: getRandomColor()      
        },
        {
          text: "hl=somethingElse",
          type: "queryParam",
          hoverText: "hovering param now",
          color: getRandomColor()
        }
      ]
    ];
    */

  }

}