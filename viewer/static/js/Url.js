import { getRandomColor } from "./functions.js";

export class Url {

  /////////////////////////////////////////
  constructor(url) {
    this.originalUrl = url || "";
    this.segments = [];
    this.page = 0;
  }


  /////////////////////////////////////////
  // analyse original and segment it
  // THIS IS REALLY GOOD: https://dmitripavlutin.com/parse-url-javascript/
  analyse() {
    console.log("Analyse was run!");

    const myUrl = new URL(this.originalUrl);
    console.log("myUrl:", myUrl);

    // http protocol
    this.segments.push({
      text: myUrl.protocol + "//", // http or https
      type: "protocol",
      color: getRandomColor(),
      isHovered: false,
      hoverArea: {
        x1: null, y1: null,
        x2: null, y2: null,
        x3: null, y3: null,
        x4: null, y4: null,
      }
    });

    // hostname (same as domain)
    this.segments.push({
      text: myUrl.hostname, // domain
      type: "domain",
      color: getRandomColor(),
      isHovered: false,
      hoverArea: {
        x1: null, y1: null,
        x2: null, y2: null,
        x3: null, y3: null,
        x4: null, y4: null,
      }
    }); 

    // putting pahtname in here
    this.segments.push({
      text: myUrl.pathname,
      type: "pathname",
      color: getRandomColor(),
      isHovered: false,
      hoverArea: {
        x1: null, y1: null,
        x2: null, y2: null,
        x3: null, y3: null,
        x4: null, y4: null,
      }
    });

    // putting search in here
    const searchBits = myUrl.search.split("&");
    for (let c=0; c < searchBits.length; c++) {
      this.segments.push({
        text: searchBits[c],
        type: "search",
        color: getRandomColor(),
        isHovered: false,
        hoverArea: {
          x1: null, y1: null,
          x2: null, y2: null,
          x3: null, y3: null,
          x4: null, y4: null,
        }
      });
      // add in the amepersand
      if (c < searchBits.length-1) {
        this.segments.push({
          text: "&",
          type: "search",
          color: getRandomColor(),
          isHovered: false,
          hoverArea: {
            x1: null, y1: null,
            x2: null, y2: null,
            x3: null, y3: null,
            x4: null, y4: null,
          }
        });        
      }
    }


    // putting hash in here
    this.segments.push({
      text: myUrl.hash,
      type: "hash",
      color: getRandomColor(),
      isHovered: false,
      hoverArea: {
        x1: null, y1: null,
        x2: null, y2: null,
        x3: null, y3: null,
        x4: null, y4: null,
      }      
    });    

    console.log("this.segments:", this.segments);
  }
  

}