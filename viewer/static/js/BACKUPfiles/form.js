/////////////////////////////////////////////
// send the search form content
const sendForm = () => {

  const form = document.getElementById("searchForm");
  // console.log("form:", form);

  // sending it!
  // https://stackoverflow.com/questions/46640024/how-do-i-post-form-data-with-fetch-api
  /*
  const data = new URLSearchParams();
  for (const pair of new FormData(searchForm)) {
    data.append(pair[0], pair[1]);
    console.log("pair[0]:", pair[0], "pair[1]:", pair[1]);
  }
  console.log("sendForm -> data:", data)
  console.log("sendForm -> data[0]:", data[0])
  */

  // validate before sending form data ! ...
  const isValidated = validateForm(form);
  if (isValidated == false) return

  let formData = new FormData();
  // formData.append('name', 'John');
  // formData.append('password', 'John123');
  formData.append('searchString', form.elements['searchString'].value); // testing
  formData.append('queryParams', form.elements['queryParams'].value); // testing
  
  // Sending the data
  fetch("/form", {
    method: 'post',
    body: formData // data,
  })
  .then(
    console.log("DONE!")
  );  
}


/////////////////////////////////////////////
// do we have the right content?
const validateForm = (form) => {

  let isValidated = true;

  console.log("form.elements['searchString']", form.elements['searchString'].value);

  // check for "" in search params
  if ( form.elements['searchString'].value == "" ) {
    console.log("ITS empty")
    formMessage("Search string is EMPTY. Please provide text for the search field", form);
    isValidated = false;
  }

  return isValidated;
}


/////////////////////////////////////////////
// display an error message in the form
const formMessage = (msg, form) => {

  // set msg wrapper to visible
  document.getElementById("formMessage").innerText = msg;
  document.getElementById("formMessageWrapper").classList.remove("closed");

}


/////////////////////////////////////////////
export {
  sendForm
}