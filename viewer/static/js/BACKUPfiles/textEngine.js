import { FontLoader } from "three/addons/loaders/FontLoader.js";


///////////////////////////////////////////
// this engine renders all the text
class textEngine {


  //////////////////////////////////////////
  // setup all vars
  constructor() {
    this.pos = { x: 100, y: 100, z: -10 }
    this.text = "demo!";

    this.fontLoader = new FontLoader();
    this.font;
    this.fontIsLoaded = false;

    this.greenMaterial;
    this.whiteMaterial;
  }

  
  //////////////////////////////////////////
  // setup the font
  loadFont(callback) {
    let _this = this;
    // load font
    this.fontLoader.load("fonts/helvetiker_regular.typeface.json", function (font) {
      _this.fontIsLoaded = true;
      _this.font = font;
      callback();
    });
  }


  //////////////////////////////////////////
  // demotext for render
  buildTestMessage = () => {
    let msg = '';
    for (let y = 0; y < 20; y++) {
      msg += "hello there internet" + "\n";
    }
    return msg;
  }


  //////////////////////////////////////////
  // Let's build a function that mimicks Richards layout
  renderResults(THREE, scene) {

    console.log("render results");

    // draw left column
    // _ divide in page results ..
    // draw right col
    // _ ...


    // left col
    for (let col1 = 0; col1 < 20; col1++) {

      let message = "https://Three.js/something/..."; // \nSimple text.\n";
      // message += this.buildTestMessage();    
  
      // generate shapes
      const shapes = this.font.generateShapes(message, 2); //  100); // 2 is size!
      const geometry = new THREE.ShapeGeometry(shapes);
      geometry.computeBoundingBox();
      const xMid = 0; // -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
      geometry.translate(xMid, col1 * -3, 0);
  
      // make shape ( N.B. edge view not visible )
      let text;
      if (col1 % 3 == 0) { text = new THREE.Mesh(geometry, this.greenMaterial); }
      else { text = new THREE.Mesh(geometry, this.whiteMaterial); }
      text.position.z = this.pos.z; // -150;
      scene.add(text);
    }

    // right col
    for (let col2 = 0; col2 < 20; col2++) {

      let message = "https://Three.js/something/...";  
      // generate shapes
      const shapes = this.font.generateShapes(message, 2);
      const geometry = new THREE.ShapeGeometry(shapes);
      geometry.computeBoundingBox();
      const xMid = 0; // -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
      geometry.translate(60, col2 * -3, 0);
  
      // make shape ( N.B. edge view not visible )
      let text;
      if (col2 % 3 == 0) { text = new THREE.Mesh(geometry, this.greenMaterial); }
      else { text = new THREE.Mesh(geometry, this.whiteMaterial); }
      text.position.z = this.pos.z; // -150;
      scene.add(text);
    }

    // testing curve
    // https://threejs.org/docs/#api/en/extras/curves/CubicBezierCurve3
    const curve = new THREE.CubicBezierCurve3(
      new THREE.Vector3( -10, 0, this.pos.z ),
      new THREE.Vector3( -5, 15, this.pos.z ),
      new THREE.Vector3( 20, 15, this.pos.z ),
      new THREE.Vector3( 10, 0, this.pos.z )
    );
    
    const points = curve.getPoints( 50 );
    const geometry = new THREE.BufferGeometry().setFromPoints( points );
    
    const material = new THREE.LineBasicMaterial( { color: 0xff0000 } );
    
    // Create the final object to add to the scene
    const curveObject = new THREE.Line( geometry, this.greenMaterial );
    scene.add(curveObject);

    // testing green cube
    const cubeGeo = new THREE.BoxGeometry(0.75, 0.75, 0.1);
    const myCube = new THREE.Mesh(cubeGeo, this.greenMaterial);
    myCube.position.set(50, 0, this.pos.z);
    scene.add(myCube);
  }


  //////////////////////////////////////////
  // render all the text to screen
  renderText(THREE, scene) {

    // some message
    let message = "Three.js\nSimple text.\n";
    message += this.buildTestMessage();    

    // generate shapes
    const shapes = this.font.generateShapes(message, 2); //  100); // 2 is size!
    const geometry = new THREE.ShapeGeometry(shapes);
    geometry.computeBoundingBox();
    const xMid = 0; // -0.5 * (geometry.boundingBox.max.x - geometry.boundingBox.min.x);
    geometry.translate(xMid, 0, 0);

    // make shape ( N.B. edge view not visible )
    const text = new THREE.Mesh(geometry, this.greenMaterial);
    text.position.z = this.pos.z; // -150;
    scene.add(text);
  }


  //////////////////////////////////////////
  // setup white and green material for shapes
  setupMaterials(THREE) {

    const whiteColor = 0xffffff;
    this.whiteMaterial = new THREE.MeshBasicMaterial({
      color: whiteColor,
      transparent: true,
      opacity: 0.8, //  0.4,
      side: THREE.DoubleSide,
    });

    const greenColor = 0x00FF81; // 0x33ff33;
    this.greenMaterial = new THREE.MeshBasicMaterial({
      color: greenColor,
      transparent: true,
      opacity: 0.8, //  0.4,
      side: THREE.DoubleSide,
    });
  }
}




///////////////////////////////////////////
export {
  textEngine
};
