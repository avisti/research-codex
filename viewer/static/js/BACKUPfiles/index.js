import { App } from "./App.js";
import { init3D, animate } from './setup3Dworld.js';
import { sendForm } from './form.js';


window.addEventListener("DOMContentLoaded", function() {

  const app = new App();
  app.init();

  // get the form and process it!
  const frm = document.getElementById("searchForm");
  frm.addEventListener('keypress', (e) => {
    if (e.keyCode === 13) { e.preventDefault(); }
  });
  frm.addEventListener("submit", (e) => {
    e.preventDefault();
    // console.log("FORM SUBMITTED!");
    sendForm();
  })

  // setup 3D
  init3D();
  animate(); 

})
