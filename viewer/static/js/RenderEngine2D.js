import { drawHoverArea, getLineTotalLength, getWinSize, handleMouseMove } from "./functions.js";


export class RenderEngine2D {


  /////////////////////////////////////////////////
  // setting up vars for renderer
  constructor() {

    this.xPos = null; // center in window
    this.yPos = null; // center in window
    this.zoomLevel = 10;
    // console.log("this.xpos, this.ypos:", this.xPos, this.yPos); // works

    // my mouse pos at all times
    this.mouse = {
      x: null, y: null
    }
    // My View Window
    this.frame = {
      x: null, // position of frame, added to general xPos
      y: null,
      xPrev: null,
      yPrev: null,
      xDiff: 0, yDiff: 0, // the distance between current and previous mouse position
      firstX: 0, firstY: 0, // these are for comparing x and y pos on mouse down and up
      lastX: 0, lastY: 0,
      isPanning: false,
      isClick: false,
      clickThreshold: 10
    };

    // text vars, standarc
    this.fontSize = 48;
    this.lineHeight = 1.35; // this.fontSize * 1.25;

    this.cnv = document.getElementById('renderCanvas');
    this.ctx = this.cnv.getContext("2d");

    // these below don't do much
    // this.ctx.mozImageSmoothingEnabled = false; // (obsolete)
    // this.ctx.webkitImageSmoothingEnabled = false;
    // this.ctx.msImageSmoothingEnabled = false;
    // this.ctx.oImageSmoothingEnabled = false;
  }


  /////////////////////////////////////////////////
  // setting the renderer up!
  init() {

    console.log("renderer init was run!");

    // mouse down/up
    window.addEventListener("mousedown", e => {
      this.frame.isPanning = true;
      this.frame.firstX = this.frame.x;
      this.frame.firstY = this.frame.y;
      this.isClick = false; // we don't know yet
    })
    window.addEventListener("mouseup", e => {
      this.frame.isPanning = false;
      this.frame.lastX = this.frame.x;
      this.frame.lastY = this.frame.y;

      // check for click distance
      const xDiff = Math.abs(this.frame.firstX - this.frame.lastX);
      const yDiff = Math.abs(this.frame.firstY - this.frame.lastY);
      if (xDiff < this.frame.clickThreshold && yDiff < this.frame.clickThreshold) {
        console.log("CLICK!");
        this.isClick = true;
      }
      else {
        this.isClick = false;
      }
    })

    // mouse down
    // https://stackoverflow.com/questions/6042202/how-to-distinguish-mouse-click-and-drag
    // __ the one with 5 answers in bottom is super ..
    window.addEventListener("mousemove", (event) => {

      // console.log("mouse is moved");
      const move = handleMouseMove(event);
      // console.log("move x, y:", move.x, move.y);

      this.mouse = { x: move.x, y: move.y }; // mouse position

      this.frame.xDiff = move.x - this.frame.xPrev;
      this.frame.yDiff = move.y - this.frame.yPrev;
      // console.log("x, y:", this.frame.x, this.frame.y);
      // console.log("x, y, prev:", this.frame.xPrev, this.frame.yPrev);
      // console.log("x, y diff: ", this.frame.xDiff, this.frame.yDiff);

      // reset prev
      this.frame.xPrev = move.x;
      this.frame.yPrev = move.y;

      // check for drag
      if (this.frame.isPanning) {
        // update the frame!!
        this.frame.x += this.frame.xDiff;
        this.frame.y += this.frame.yDiff;          
      }
    })    
  }


  /////////////////////////////////////////////////
  // put the results on canvas!
  draw(resultsLeft, resultsRight) {

    // clear canvas
    this.ctx.clearRect(0, 0, this.cnv.width, this.cnv.height);
    const lh = this.getLineHeight();
    const centerXOffset = lh * 2.5;

    // draw the dots and the bezier curves
    this.drawConnections(resultsLeft, resultsRight);

    // get position with frame position added!
    const xPos = this.xPos + this.frame.x;
    const yPos = this.yPos + this.frame.y;


    // Draw the text on the left!
    this.ctx.font = `${this.fontSize}px monospace`;
    for (let c = 0; c < resultsLeft.length; c++) {

      // console.log("resultsLeft[c]:", resultsLeft[c]);
      const lineTotalLength = getLineTotalLength(resultsLeft[c].segments, this.ctx);
      // console.log("lineTotalLength:", lineTotalLength);
      const leftX = xPos - lineTotalLength - centerXOffset;

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsLeft[c].segments.length; cc++) {

        xOffset += (cc > 0) ? this.ctx.measureText(resultsLeft[c].segments[cc-1].text).width : 0 ;
        // console.log("xOffset:", xOffset); // works
        this.ctx.fillStyle = resultsLeft[c].segments[cc].color;
        this.ctx.fillText(resultsLeft[c].segments[cc].text, leftX + xOffset, yPos + (c * lh));

        // check for hovered
        if (resultsLeft[c].segments[cc].isHovered) {
          drawHoverArea(resultsLeft[c].segments[cc].hoverArea, this.ctx);
        }        
      }
    }

    
    // Draw the text on the right!
    // this.ctx.font = `${this.fontSize}px serif`;
    for (let c = 0; c < resultsRight.length; c++) {

      const lineTotalLength = getLineTotalLength(resultsRight[c].segments, this.ctx);
      // console.log("lineTotalLength:", lineTotalLength);
      const leftX = xPos + centerXOffset;

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsRight[c].segments.length; cc++) {
        xOffset += (cc > 0) ? this.ctx.measureText(resultsRight[c].segments[cc-1].text).width : 0 ;
        this.ctx.fillStyle = resultsRight[c].segments[cc].color;
        this.ctx.fillText(resultsRight[c].segments[cc].text, leftX + xOffset, yPos + (c * lh));

        // check for hovered
        if (resultsRight[c].segments[cc].isHovered) {
          drawHoverArea(resultsRight[c].segments[cc].hoverArea, this.ctx);
        }
      }
    }
    
    // TEXT RENDERING - LAST POST! ...
    // https://stackoverflow.com/questions/40066166/canvas-text-rendering-blurry
  }



  /////////////////////////////////////////////////
  // draw lines between similar Urls
  drawConnections(resultsLeft, resultsRight) {

    // get position with frame position added!
    const xPos = this.xPos + this.frame.x;
    const yPos = this.yPos + this.frame.y;    

    this.ctx.fillStyle = "white";
    this.ctx.lineWidth = Math.round(this.fontSize / 30); 
    const lh = this.getLineHeight();
    const xOffset = (lh * 2); // 1.65);
    const yOffset = (lh / 4.5);
    const dotSize = (lh / 5);

    // Lets try the left side first
    for (let c = 0; c < resultsLeft.length; c++) {

      // test distance to other dot
      const distance = Math.round((Math.random() * 6) - 3); // testing connections

      this.ctx.fillRect(
        xPos - xOffset - (dotSize / 2),
        yPos + (c * lh) - yOffset - (dotSize / 2),
        dotSize, dotSize
      );
      // right side dots
      this.ctx.fillRect(
        xPos + xOffset - (dotSize / 2),
        yPos + (c * lh) - yOffset - (dotSize / 2),
        dotSize, dotSize
      );

      // bezier curve here
      // https://www.w3schools.com/jsref/canvas_beziercurveto.asp
      this.ctx.strokeStyle = "white";

      this.ctx.beginPath();
      this.ctx.moveTo(
        xPos - xOffset,
        yPos + (c * lh) - yOffset
      );
      this.ctx.bezierCurveTo(
        xPos,
        yPos + (c * lh) - yOffset,
        xPos,
        yPos + (c * lh) + (lh * distance) - yOffset,
        xPos + xOffset,
        yPos + (c * lh) + (lh * distance) - yOffset
      );
      this.ctx.stroke();
      this.ctx.closePath();
    }
  }  


  /////////////////////////////////////////////////
  // calc all hover areas for all text segements!
  calculateHoverAreas(resultsLeft, resultsRight) {

    const lh = this.getLineHeight();
    const centerXOffset = lh * 2.5;    

    // get position with frame position added!
    const xPos = this.xPos + this.frame.x;
    const yPos = this.yPos + this.frame.y;        

    // DRAW BOXES on the left!
    for (let c = 0; c < resultsLeft.length; c++) {

      const lineTotalLength = getLineTotalLength(resultsLeft[c].segments, this.ctx);
      const leftX = xPos - lineTotalLength - centerXOffset;

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsLeft[c].segments.length; cc++) {

        xOffset += (cc > 0) ? this.ctx.measureText(resultsLeft[c].segments[cc-1].text).width : 0 ;

        // get a random color, and draw a box with it!
        let w = this.ctx.measureText(resultsLeft[c].segments[cc].text).width;

        // set the hoverArea
        resultsLeft[c].segments[cc].hoverArea = {
          x1: leftX + xOffset, y1: yPos + (c * lh) - (lh/1.875),
          x2: leftX + xOffset + w, y2: yPos + (c * lh) - (lh/1.875),
          x3: leftX + xOffset + w, y3: yPos + (c * lh) + (lh/5.875),
          x4: leftX + xOffset, y4: yPos + (c * lh) + (lh/5.875)
        }
        // drawHoverArea(resultsLeft[c].segments[cc].hoverArea, this.ctx);
      }
    }
    
    // DRAW Boxes on the right!
    for (let c = 0; c < resultsRight.length; c++) {

      const leftX = xPos + centerXOffset;

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsRight[c].segments.length; cc++) {

        xOffset += (cc > 0) ? this.ctx.measureText(resultsRight[c].segments[cc-1].text).width : 0 ;

        // get a random color, and draw a box with it!
        let w = this.ctx.measureText(resultsRight[c].segments[cc].text).width;

        // set the hoverArea
        resultsRight[c].segments[cc].hoverArea = {
          x1: leftX + xOffset, y1: yPos + (c * lh) - (lh/1.875),
          x2: leftX + xOffset + w, y2: yPos + (c * lh) - (lh/1.875),
          x3: leftX + xOffset + w, y3: yPos + (c * lh) + (lh/5.875),
          x4: leftX + xOffset, y4: yPos + (c * lh) + (lh/5.875)
        }
        // drawHoverArea(resultsRight[c].segments[cc].hoverArea, this.ctx);
      }
    }    
  }


  /////////////////////////////////////////////////
  // Am i hovering on some part of the url? -then show it
  drawHover(resultsLeft, resultsRight) {

    // CHECK BOXES on the left!
    for (let c = 0; c < resultsLeft.length; c++) {

      // if y doesnt fit, then skip and set all to not hovered
      const top = resultsLeft[c].segments[0].hoverArea.y1;
      const bottom = resultsLeft[c].segments[0].hoverArea.y3;
      // console.log("this.mouse.y:", this.mouse.y);
      // console.log("top, bottom:", top, bottom);

      if (this.mouse.y > top && this.mouse.y < bottom ) { 
        // console.log("HOVERING!!!")
        for (let cc = 0; cc < resultsLeft[c].segments.length; cc++) {
          const segm = resultsLeft[c].segments[cc];
          const left = segm.hoverArea.x1;
          const right = segm.hoverArea.x2;
          if (this.mouse.x > left && this.mouse.x < right) { segm.isHovered = true; }
          else { segm.isHovered = false; }
        }        
      }
      else {
        // set all to not hovered
        for (let cc = 0; cc < resultsLeft[c].segments.length; cc++) {
          resultsLeft[c].segments[cc].isHovered = false;
        }
      }
    }
    
    // CHECK Boxes on the right!
    for (let c = 0; c < resultsRight.length; c++) {

      const top = resultsRight[c].segments[0].hoverArea.y1;
      const bottom = resultsRight[c].segments[0].hoverArea.y3;

      if (this.mouse.y > top && this.mouse.y < bottom ) { 
        // console.log("HOVERING!!!")
        for (let cc = 0; cc < resultsRight[c].segments.length; cc++) {
          const segm = resultsRight[c].segments[cc];
          const left = segm.hoverArea.x1;
          const right = segm.hoverArea.x2;
          if (this.mouse.x > left && this.mouse.x < right) { segm.isHovered = true; }
          else { segm.isHovered = false; }
        }
      }
      else {
        for (let cc = 0; cc < resultsRight[c].segments.length; cc++) {
          resultsRight[c].segments[cc].isHovered = false;
        }
      }
    }
  }


  /////////////////////////////////////////////////
  // Listening for mouse wheel and then doing zoom!
  // https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event
  updateZoom(event) {
    console.log("mousewheel was used!");
    // event.preventDefault();
    this.zoomLevel += event.deltaY * -0.02;
    document.getElementById("zoomLevel").innerText = this.zoomLevel;

    this.fontSize += event.deltaY * -0.02;
  
    // Restrict this.zoomLevel
    // this.zoomLevel = Math.min(Math.max(0.125, this.zoomLevel), 4);
    // Apply this.zoomLevel transform
    // el.style.transform = `this.zoomLevel(${this.zoomLevel})`;    
  }


  /////////////////////////////////////////////////
  // when resizing window, resize canvas! 
  resize() {
    const size = getWinSize();
    this.cnv.width = size.width;
    this.cnv.height = size.height;
    this.xPos = size.width / 2;
    this.yPos = size.height / 2;
  }


  /////////////////////////////////////////////////
  // make calc lineHeight simpler! ..
  getLineHeight() {
    return this.fontSize * this.lineHeight;
  }


}