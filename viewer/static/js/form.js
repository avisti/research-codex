/////////////////////////////////////////////
// send the search form content
const sendForm = async () => {

  const form = document.getElementById("searchForm");

  // validate before sending form data ! ...
  const isValidated = validateForm(form);
  if (!isValidated) return

  let formData = new FormData();

  // how to append files to FormData
  // https://plainenglish.io/blog/how-to-append-javascript-data-to-formdata
  const file1 = document.getElementById("file1").files[0];
  console.log("file1:", file1);
  formData.append('file1', file1, file1.name);
  const file2 = document.getElementById("file2").files[0];
  console.log("file2:", file2);
  formData.append('file2', file2, file2.name);

  formData.append('searchString', form.elements['searchString'].value); // testing
  formData.append('queryParams', form.elements['queryParams'].value); // testing
  
  // Sending the data
  // https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#supplying_request_options
  const options = {
    method: "POST", // *GET, POST, PUT, DELETE, etc.
    // mode: "cors", // no-cors, *cors, same-origin
    // cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
    // credentials: "same-origin", // include, *same-origin, omit
    headers: {
      "Content-Type": "multipart/form-data",
      // "Content-Type": "application/json",
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    // redirect: "follow", // manual, *follow, error
    // referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
    body: formData, // JSON.stringify(formData), // body data type must match "Content-Type" header
  }

  await fetch("/sendFiles", options)
    .then(
      console.log("DONE!")
    );  
}


/////////////////////////////////////////////
// do we have the right content?
const validateForm = (form) => {

  let isValidated = true;

  console.log("form.elements['searchString']", form.elements['searchString'].value);
  console.log("form.elements['file1']", form.elements['file1'].value);

  /*
  // check for "" in search params
  if ( form.elements['searchString'].value == "" ) {
    console.log("ITS empty")
    formMessage("Search string is EMPTY. Please provide text for the search field", form);
    isValidated = false;
  }
  */

  return isValidated;
}


/////////////////////////////////////////////
// display an error message in the form
const formMessage = (msg, form) => {
  // set msg wrapper to visible
  document.getElementById("formMessage").innerText = msg;
  document.getElementById("formMessageWrapper").classList.remove("closed");
}


/////////////////////////////////////////////
export {
  sendForm
}