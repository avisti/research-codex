////////////////////////////////////////////////////////////7
// draw the hover area on screen
const drawHoverArea = (area, ctx) => {

  ctx.strokeStyle = "white"; // getRandomColor();
  ctx.beginPath();
  ctx.moveTo(area.x1, area.y1);
  ctx.lineTo(area.x2, area.y2);
  ctx.lineTo(area.x3, area.y3);
  ctx.lineTo(area.x4, area.y4);
  ctx.lineTo(area.x1, area.y1);
  ctx.stroke();

}


////////////////////////////////////////////////////////////
// ctx measure the argument line, and return it
const getLineTotalLength = (line, ctx) => {
  // console.log("line:", line);
  let length = 0;
  for (let c = 0; c < line.length; c++) { length += ctx.measureText(line[c].text).width; }
  return length;
}


////////////////////////////////////////////////////////////
// get the window size
const getWinSize = () => {
  const width = window.innerWidth ||
  document.documentElement.clientWidth ||
  document.body.clientWidth;

  const height = window.innerHeight ||
    document.documentElement.clientHeight ||
    document.body.clientHeight;

  return { width, height }
}


////////////////////////////////////////////////////////////
// handle the mouse move
const handleMouseMove = (event) => {

  var eventDoc, doc, body;
  event = event || window.event; // IE-ism

  // If pageX/Y aren't available and clientX/Y are,
  // calculate pageX/Y - logic taken from jQuery.
  // (This is to support old IE)
  if (event.pageX == null && event.clientX != null) {
      eventDoc = (event.target && event.target.ownerDocument) || document;
      doc = eventDoc.documentElement;
      body = eventDoc.body;

      event.pageX = event.clientX +
        (doc && doc.scrollLeft || body && body.scrollLeft || 0) -
        (doc && doc.clientLeft || body && body.clientLeft || 0);
      event.pageY = event.clientY +
        (doc && doc.scrollTop  || body && body.scrollTop  || 0) -
        (doc && doc.clientTop  || body && body.clientTop  || 0 );
  }

  // Use event.pageX / event.pageY here
  return {
    x: event.pageX,
    y: event.pageY
  }
}


////////////////////////////////////////////////////////////
// get a random color
const getRandomColor = () => {
  const letters = '0123456789ABCDEF';
  let color = '#';
  for (var i = 0; i < 6; i++) { color += letters[Math.floor(Math.random() * 16)]; }
  return color;
}


////////////////////////////////////////////////////////////
export {
  drawHoverArea,
  getLineTotalLength,
  getWinSize,
  handleMouseMove,
  getRandomColor
}