import { App } from "./App.js";
import { sendForm } from './form.js';


window.addEventListener("DOMContentLoaded", function() {

  const app = new App();
  app.init();

  // get the form and process it!
  const frm = document.getElementById("searchForm");
  frm.addEventListener('keypress', (e) => {
    if (e.keyCode === 13) { e.preventDefault(); }
  });
  frm.addEventListener("submit", async (e) => {
    e.preventDefault();
    await sendForm();
  })


  // setup resizing
  window.onresize = (event) => app.renderer.resize();
  app.renderer.resize();

  // on scroll wheel
  window.onwheel = (event) => {
    // event.preventDefault();
    app.renderer.updateZoom(event);
  };


  // draw loop, make better! ..
  const loop = () => {
    requestAnimationFrame(loop);
    app.renderer.draw(app.txt.resultsLeft, app.txt.resultsRight);
    app.renderer.calculateHoverAreas(app.txt.resultsLeft, app.txt.resultsRight);
    app.renderer.drawHover(app.txt.resultsLeft, app.txt.resultsRight);
  }
  loop();

})
