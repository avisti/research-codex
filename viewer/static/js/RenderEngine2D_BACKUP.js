import { getLineTotalLength, getWinSize } from "./functions.js";


export class RenderEngine2D {


  /////////////////////////////////////////////////
  // setting up vars for renderer
  constructor() {
    this.xPos = null; // center in window
    this.yPos = null; // center in window
    this.zoomLevel = 10;
    // console.log("this.xpos, this.ypos:", this.xPos, this.yPos); // works

    // text vars, standarc
    this.fontSize = 48;
    this.lineHeight = 1.35; // this.fontSize * 1.25;

    this.cnv = document.getElementById('renderCanvas');
    this.ctx = this.cnv.getContext("2d");

    // these below don't do much
    // this.ctx.mozImageSmoothingEnabled = false; // (obsolete)
    // this.ctx.webkitImageSmoothingEnabled = false;
    // this.ctx.msImageSmoothingEnabled = false;
    // this.ctx.oImageSmoothingEnabled = false;
  }


  /////////////////////////////////////////////////
  // put the results on canvas!
  draw(resultsLeft, resultsRight) {

    // clear canvas
    this.ctx.clearRect(0, 0, this.cnv.width, this.cnv.height);
    // this.ctx.fontAlign = "right";


    // draw the square "Dots" ON LEFT SIDE! ..
    this.ctx.fillStyle = "white";
    for (let c = 0; c < resultsLeft.length; c++) {

      const lh = this.getLineHeight();

      this.ctx.fillRect(
        this.xPos - (lh * 1.65),
        this.yPos + (c * lh) - (lh / 3),
        this.fontSize / 4,
        this.fontSize / 4
      );

      // bezier curve here
      // https://www.w3schools.com/jsref/canvas_beziercurveto.asp
      this.ctx.strokeStyle = "white";

      this.ctx.beginPath();
      this.ctx.moveTo(
        this.xPos - (lh * 1.65),
        this.yPos + (c * lh) - (lh / 4)
      );
      this.ctx.bezierCurveTo(
        this.xPos,
        this.yPos + (c * lh) - (lh / 4),
        this.xPos,
        this.yPos + (c * lh) + (lh * 3),
        this.xPos + (lh * 1.65),
        this.yPos + (c * lh) + (lh * 3)
      );
      this.ctx.stroke();
      this.ctx.closePath();

      /*
      this.ctx.beginPath();
      this.ctx.moveTo(
        this.xPos - (lh * 1.65),
        this.yPos + (c * lh) - (lh / 3)
      );
      this.ctx.bezierCurveTo(
        this.xPos - (lh * 1.65),
        this.yPos + (c * lh) + (lh * 3), // - (lh / 3), // 100,
        this.xPos + (lh * 1.65),
        this.yPos + (c * lh) + (lh * 3),
        this.xPos + (lh * 1.65),
        this.yPos + (c * lh) + (lh * 6) // this.yPos + (c * lh) - (lh / 3)
      );
      this.ctx.stroke();
      this.ctx.closePath();
      */
    }


    // Draw the text on the left!
    this.ctx.font = `${this.fontSize}px monospace`;
    for (let c = 0; c < resultsLeft.length; c++) {

      // console.log("resultsLeft[c]:", resultsLeft[c]);
      const lineTotalLength = getLineTotalLength(resultsLeft[c].segments, this.ctx);
      // console.log("lineTotalLength:", lineTotalLength);
      const leftX = this.xPos - lineTotalLength - (this.getLineHeight() * 2);

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsLeft[c].segments.length; cc++) {

        xOffset += (cc > 0) ? this.ctx.measureText(resultsLeft[c].segments[cc-1].text).width : 0 ;
        // console.log("xOffset:", xOffset); // works
        this.ctx.fillStyle = resultsLeft[c].segments[cc].color;
        this.ctx.fillText(resultsLeft[c].segments[cc].text, leftX + xOffset, this.yPos + (c * this.getLineHeight()));
      }
    }

    
    // Draw the text on the right!
    this.ctx.font = `${this.fontSize}px serif`;
    for (let c = 0; c < resultsRight.length; c++) {

      const lineTotalLength = getLineTotalLength(resultsRight[c].segments, this.ctx);
      // console.log("lineTotalLength:", lineTotalLength);
      const leftX = this.xPos + (this.getLineHeight() * 2);

      // run through url segments
      let xOffset = 0;
      for (let cc = 0; cc < resultsRight[c].segments.length; cc++) {
        xOffset += (cc > 0) ? this.ctx.measureText(resultsRight[c].segments[cc-1].text).width : 0 ;
        this.ctx.fillStyle = resultsRight[c].segments[cc].color;
        this.ctx.fillText(resultsRight[c].segments[cc].text, leftX + xOffset, this.yPos + (c * this.getLineHeight()));
      }
    }



    // text width:
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_text
    
    // TEXT RENDERING - LAST POST! ...
    // https://stackoverflow.com/questions/40066166/canvas-text-rendering-blurry
  }



  /////////////////////////////////////////////////
  // Listening for mouse wheel and then doing zoom!
  // https://developer.mozilla.org/en-US/docs/Web/API/Element/wheel_event
  updateZoom(event) {
    console.log("mousewheel was used!");
    // event.preventDefault();
    this.zoomLevel += event.deltaY * -0.02;
    document.getElementById("zoomLevel").innerText = this.zoomLevel;

    this.fontSize += event.deltaY * -0.02;
  
    // Restrict this.zoomLevel
    // this.zoomLevel = Math.min(Math.max(0.125, this.zoomLevel), 4);
    // Apply this.zoomLevel transform
    // el.style.transform = `this.zoomLevel(${this.zoomLevel})`;    
  }


  /////////////////////////////////////////////////
  // when resizing window, resize canvas! 
  resize() {
    const size = getWinSize();
    this.cnv.width = size.width;
    this.cnv.height = size.height;
    this.xPos = size.width / 2;
    this.yPos = size.height / 2;
  }


  /////////////////////////////////////////////////
  // make calc lineHeight simpler! ..
  getLineHeight() {
    return this.fontSize * this.lineHeight;
  }


}