# Viewer

This is the node.js server, with the custom build software, that visualises user searches...  

This could be using fastify + browser APIs to visualize.  

**This app needs to:**  
_ give users input fields for two files to compare. (for now)  
__ This is important, since we haven't found a good way to fully automate the scraping of search engines from specific machines. Maybe controlling a search in a Iframe could work?, but it hasn't been tested yet.  
_ The App must store the result as an .html file on the server. (Remember: This is running on the user's machine, locally)  
_ ---
